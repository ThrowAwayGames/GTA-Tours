<div class="container" id="portfolio">
	<div class="arrow-container arrow-container-left" id="arrow-white-l"></div>
	<div class="arrow-container arrow-container-right" id="arrow-grey-r"></div>
	<div class="page-box page-box-h-1">
		<div class="vert-line vert-line-h-1"></div>
		<h1 class="page-title">PORTFOLIO</h1>
		<div class="vert-line vert-line-h-2"></div>
	</div>
	<div class="content-container-1">
		<div class="left-content-box">
			<img class="content-box-img" src="./images/pics/033-gta-tours-gallery.jpg"/>
		</div>
		<div class="left-content-box right-content-box">
			<h2 class="left-box-title">TITLE OF THIS ITEM</h2>
			<p class="left-box-text">
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
				Aenean commodo ligula eget dolor. Aenean massa. 
				Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
				Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. 
				Nulla consequat massa quis enim.
			</p>
			<div class="uni-button">
				BUTTON
			</div>
		</div>
	</div>
	<div class="page-box page-box-h-2">
		<div class="vert-line vert-line-h-1"></div>
		<div class="down-arrow-grey"></div>
	</div>
</div>
