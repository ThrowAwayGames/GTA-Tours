<div class="container container-gray jcarousel-wrapper">
	<div class="arrow-container arrow-container-left jcarousel-prev" id="arrow-white-l"></div>
	<div class="arrow-container arrow-container-right jcarousel-next" id="arrow-grey-r"></div>
	<div class="page-box page-box-h-1">
		<div class="vert-line vert-line-white vert-line-h-1"></div>
		<h1 class="page-title page-title-white">DEMO</h1>
		<div class="vert-line vert-line-white vert-line-h-2"></div>
	</div>
	<div class="content-container-1 jcarousel jcarousel-setwidth" id="demo">
		<ul class="jcarousel-list">

			<li class="jcarousel-item active">
				<div class="left-content-box">
					<img class="content-box-img" src="http://www.gtatour.ca/uploads/017219.jpg"/>
				</div>
				<div class="left-content-box right-content-box">
					<h2 class="left-box-title">Package 1</h2>
					<p class="left-box-text">
						 30-40 High quality HDR images (high dynamic range)
					</p>
					<a href="http://www.gtatour.ca/1116" class="uni-button" target="_blank">
						VIEW
					</a>
				</div>
			</li>
			<li class="jcarousel-item">
				<div class="left-content-box">
					<img class="content-box-img" src="http://www.gtatour.ca/uploads/134344.jpg"/>
				</div>
				<div class="left-content-box right-content-box">
					<h2 class="left-box-title">Package 2</h2>
					<p class="left-box-text">
						30-40 High quality HDR images (high dynamic range) with website links featuring all photos
					</p>
					<a href="http://www.gtatour.ca/1132/" class="uni-button" target="_blank">
						VIEW
					</a>
				</div>
			</li>
			<!--<li class="jcarousel-item">
				<div class="left-content-box">
					<img class="content-box-img" src="./images/pics/001-gta-tours-gallery.jpg"/>
				</div>
				<div class="left-content-box right-content-box">
					<h2 class="left-box-title">17 Douglas Cres</h2>
					<p class="left-box-text">
						17 Douglas Cres<br>
						Toronto, ON
					</p>
					<a href="http://www.gtatour.ca/899" class="uni-button">
						VIEW
					</a>
				</div>
			</li>
			<li class="jcarousel-item">
				<div class="left-content-box">
					<img class="content-box-img" src="./images/pics/001-gta-tours-gallery.jpg"/>
				</div>
				<div class="left-content-box right-content-box">
					<h2 class="left-box-title">84 Cedar Brae Blvd</h2>
					<p class="left-box-text">
						84 Cedar Brae Blvd<br>
						Scarborough, ON<br>
						M1J 2K5
					</p>
					<a href="http://www.gtatour.ca/899" class="uni-button">
						VIEW
					</a>
				</div>
			</li>-->

			<li class="jcarousel-item active">
				<div class="left-content-box">
					<iframe src="https://my.matterport.com/show/?m=JCUDYLojbU2" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="left-content-box right-content-box">
					<h2 class="left-box-title">Construction Site</h2>
					<p class="left-box-text"></p>
				</div>
			</li>


			<li class="jcarousel-item active">
				<div class="left-content-box">
					<iframe src="https://my.matterport.com/show/?m=9dwWy8VsSw5" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="left-content-box right-content-box">
					<h2 class="left-box-title">Entertainment Location</h2>
					<p class="left-box-text"></p>
				</div>
			</li>	

		</ul>
	</div>
	<div class="page-box page-box-h-2">
		<div class="vert-line vert-line-white vert-line-h-1"></div>
		<div class="down-arrow-grey"></div>
	</div>
</div>
