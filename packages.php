<div class="container" id="packages">
	<div class="page-box page-box-h-1">
		<div class="vert-line vert-line-h-1"></div>
		<h1 class="page-title">PACKAGES</h1>
		<div class="vert-line vert-line-h-2"></div>
	</div>
	<div class="content-container-2">

		<div class="copy">
	  <p>
	    Specializing in high quality condo, residential, and commercial photography shot in HDR (high dynamic range)
	    Most buyers see the property they want on-line first!
	    Let GTATOURS.CA provide the best looking online galleries and superior photography to impress your seller!
	  </p>


		<h3>Add-Ons:</h3>
			<ul>
				<li>	Additional 25-30 photos: $25</li>
				<li>	Your own dot com: $29. <i>Market your listing more effectively with its own dot com website. EXAMPLE (www.358dufferin.com). Great for signage or feature sheets. (Valid for one year)</i></li>
				<li>	Up to 10 Mattertags (Packages 3 & 4 only): $14.99</li>
				<li>	Photo shoots after 5 pm and on the weekends are regular price PLUS 50%.</li>
				<li>	<i>Distance &amp; parking fees may apply</i></li>
			</ul>
	</div>

		<div class="package-row">
			<div class="package-icon package-align-l" id="package-icon-1"></div>
			<div class="package-content package-align-r-2">
				<h3 class="pkg-box-title">PACKAGE 1 - $99</h3>
				<div class="pkg-box-text">
					<h3>Still Photos</h3>
					<ul>
						<li>Colour corrected images shot in HDR</li>
						<li>High resolution photos for feature sheets</li>
						<li>Pre-sized images for MLS</li>
						<li>24-hour turnaround</li>
						<li>Approx. 40 photos of exterior/interior of the property</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="package-row">
			<div class="package-icon package-align-r" id="package-icon-2"></div>
			<div class="package-content package-align-l-2">
				<h3 class="pkg-box-title pkg-box-align-l">PACKAGE 2 - $128</h3>
				<div class="pkg-box-text pkg-box-align-l">
					<h3>Still Photos + Website Links* + Slideshow</h3>
					<ul>
						<li>Colour corrected images shot in HDR</li>
						<li>High resolution photos for feature sheets</li>
						<li>Pre-sized images for MLS</li>
						<li>24-hour turnaround</li>
						<li>Approx. 40 photos of exterior/interior of the property</li>
						<li>One-year web hosting. Websites are branded and unbranded (for MLS)<br/>
							<em>*Example: http://www.gtatour.ca/1376/0</em>
						</li>
						<li>YouTube Slideshow featuring up to 30 photos / 2 minutes & MP4 file (example below)</li>
					</ul>
					<p>
					</p>
				</div>
			</div>
		</div>
		<div class="package-row">
			<div class="package-icon package-align-l" id="package-icon-3"></div>
			<div class="package-content package-align-r-2">
				<h3 class="pkg-box-title">PACKAGE 3 - Starting at $329</h3>
				<div class="pkg-box-text">
					<h3>3-D Virtual Tour* + Still Photos + Website Links** + Slideshow</h3>
					<ul>
						<li>Complete 3-D coverage of property interior Branded &amp; Unbranded link <br><em>*example: https://mymatterport.com/show/?m=AZoiwzjU7pz&amp;brand=1&amp;hl=1&amp;play=1</em></li>
						<li>Colour corrected images shot in HDR</li>
						<li>High resolution photos for feature sheets</li>
						<li>Pre-sized images for MLS</li>
						<li>Approx. 40 photos of exterior/interior of the property</li>
						<li>24-hour turnaround</li>
						<li>One year single property webpage branded & unbranded <br>
							<em>**Website Link example: http://www.gtatour.ca/1376/0</em>
						</li>
						<li>YouTube Slideshow featuring up to 30 photos / 2 minutes &amp; MP4 file (example below)</li>
					</ul>
					<p><b>
						Up to 1,000 ft2 - $329 (approx. 1 hour shoot time required)<br><br>
						1,000 – 2,500 ft2 - $449 (approx. 1 &#xBD; hours shoot time required)<br><br>
						$89 per 1,000 ft2 thereafter (2 or more hours shoot time may be required)</b>
						<br><br>
						<i>Exact square footage of the home must be provided prior to booking Package #3<br><br>						<br><br>
						Interior MUST be still & free of individuals</i>
					</p>
				</div>
				<!--<div class="uni-button uni-button-small btn-more">
					MORE...
				</div>-->
			</div>
		</div>
		<div class="package-row">
			<div class="package-icon package-align-r" id="package-icon-4"></div>
			<div class="package-content package-align-l-2">
				<h3 class="pkg-box-title pkg-box-align-l">PACKAGE 4 - Starting at $379</h3>
				<div class="pkg-box-text pkg-box-align-l">
					<h3>3-D Virtual Tour* + Floor Plans + Still Photos + Website Links** + Slideshow</h3>
					<ul>
						<li>Complete 3-D coverage of property interior Branded &amp; Unbranded link <br><em>*example: https://mymatterport.com/show/?m=AZoiwzjU7pz&amp;brand=1&amp;hl=1&amp;play=1</em></li>
						<li>Floor Plans provided in PDF within 2 business days (room sizes &amp; square footage provided with 99% accuracy) </li>
						<li>Colour corrected images shot in HDR</li>
						<li>High resolution photos for feature sheets</li>
						<li>Pre-sized images for MLS</li>
						<li>Approx. 40 photos of exterior/interior of the property</li>
						<li>24-hour turnaround</li>
						<li>One year single property webpage branded &amp; unbranded <br><em>**Website Link example: http://www.gtatour.ca/1376/0</em></li>
						<li>YouTube Slideshow featuring up to 30 photos / 2 minutes &amp; MP4 file (example below)</li>
					</ul>
					<p><b>
						Up to 1,000 ft2 - $379 (approx. 1 &#xBD; hours shoot time required)<br><br>
						1, 000 – 2,500 ft2 - $489 (approx. 2 &#xBD; hours shoot time required)<br><br>
						$119 per 1,000 ft2 thereafter (3 or more hours shoot time may be required)</b>
						<br><br>
						<i>Exact square footage of the home must be provided prior to booking Package #4<br><br>
						Interior MUST be still & free of individuals</i>
					</p>
				</div>
				<!--<div class="uni-button uni-button-small uni-button-align-r btn-more">
					MORE...
				</div>-->
			</div>
		</div>
	</div>
	<div class="page-box page-box-h-2">
		<div class="vert-line vert-line-h-1"></div>
		<div class="down-arrow-grey"></div>
	</div>
</div>
