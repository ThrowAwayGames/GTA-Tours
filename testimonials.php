<div class="container jcarousel-wrapper" id="testimonials">
	<div class="arrow-container arrow-container-left jcarousel-prev" id="arrow-white-l"></div>
	<div class="arrow-container arrow-container-right jcarousel-next" id="arrow-white-r"></div>

	<div class="page-box page-box-h-1">
		<div class="vert-line vert-line-white vert-line-h-1"></div>
		<h1 class="page-title">TESTIMONIALS</h1>
		<div class="vert-line vert-line-white vert-line-h-2"></div>
	</div>
	<div class="content-container-3">
		<div class="jcarousel jcarousel-setwidth jauto">
			<ul class="jcarousel-list testimonials-carousel">
				<li class="jcarousel-item active">
					<div class="star-laurel-icon"></div>
					<h3 class="quoted-text">
						"Seller loved the pictures. All of us at the office think they are amazing – well done. I think my seller will get you a lot of business from those pictures.  Thanks so much for all your hard work. House did sell for $892,500.00"
					</h3>
					<p class="signature-text">
						-Joan Manuel, Sales Rep, Royal Lepage Signature Realty
					</p>
				</li>
				<li class="jcarousel-item active">
					<div class="star-laurel-icon"></div>
					<h3 class="quoted-text">
						"We are SO impressed with the photos­ thank you so so much!! Seriously, I've dealt with a lot of photographers and i have never gotten photos back so fast, such good quality images."
					</h3>
					<p class="signature-text">
						-Ashley Farber, Sales Rep, Forest Hill Real Estate Inc.
					</p>
				</li>
				<li class="jcarousel-item">
					<div class="star-laurel-icon"></div>
					<h3 class="quoted-text">
						"The Property Brothers TV show is filming on Friday for their show which will air in a few weeks. The producers saw the photos on line and loved them. Then want to use the house in one of their episodes."
					</h3>
					<p class="signature-text">
						-Clint Harder, Sales Rep, Royal Lepage Signature Realty
					</p>
				</li>
				<li class="jcarousel-item">
					<div class="star-laurel-icon"></div>
					<h3 class="quoted-text">
						"Your work is awesome and definitely will set up Trump Towers shoot one of these days..."
					</h3>
					<p class="signature-text">
						-Tonille Giovis, Sales Manager, York Fabrica
					</p>
				</li>
				<li class="jcarousel-item">
					<div class="star-laurel-icon"></div>
					<h3 class="quoted-text">
						"These photos are amazing :):) you are a camera wizard! And PLEASE use this direct quote as a testimonial on your website!"
					</h3>
					<p class="signature-text">
						-Rebecca Himelfarb, Administrative Manager, Forest Hill Real Estate Inc. Yorkville
					</p>
				</li>
			</ul>
		</div>
		<div class="selector-box jcarousel-pagination">
			<div class="select-item jcarousel-pagination-item active"></div>
			<div class="select-item jcarousel-pagination-item"></div>
			<div class="select-item jcarousel-pagination-item"></div>
			<div class="select-item jcarousel-pagination-item"></div>
			<div class="select-item jcarousel-pagination-item"></div>
		</div>
	</div>
	<div class="page-box page-box-h-2">
		<div class="vert-line vert-line-white vert-line-h-1"></div>
		<div class="down-arrow-grey down-arrow-white"></div>
	</div>
</div>
