<header id="header">
	<div id="logo-box">
		<a href="#" id="logo"></a>
		<div class="menu-btn"></div>
	</div>
	
</header>
<nav class="nav nav-open">
	<ul id="ul1">
		<li class="nav-btn"><a href="#services">3D Virtual Tours</a></li>
		
		<li class="nav-btn"><a href="#gallery">GALLERY</a></li>		
		<li class="nav-btn"><a href="#packages">PACKAGES</a></li>
		<li class="nav-btn"><a href="#demo">DEMO</a></li>
		<li class="nav-btn"><a href="#testimonials">testimonials</a></li>
		<li class="nav-btn"><a href="#">BLOG</a></li>
		<li class="nav-btn"><a href="#contact">CONTACT</a></li>
	</ul>
</nav>
