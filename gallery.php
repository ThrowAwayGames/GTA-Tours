<div class="container container-gray jcarousel-wrapper" id="gallery">

	<div class="page-box page-box-h-1">
		<div class="vert-line vert-line-white vert-line-h-1"></div>
		<h1 class="page-title page-title-white">GALLERY</h1>
		<div class="selector-box jcarousel-pagination">
			<div class="select-item jcarousel-pagination-item active"></div>
			<div class="select-item jcarousel-pagination-item"></div>
			<div class="select-item jcarousel-pagination-item"></div>
			<div class="select-item jcarousel-pagination-item"></div>
			<div class="select-item jcarousel-pagination-item"></div>
			<div class="select-item jcarousel-pagination-item"></div>
		</div>
	</div>
	<div class="content-container-1">
		<div class="left-content-box gallery-left">
			<div class="arrow-container arrow-container-left jcarousel-prev" id="arrow-white-l"></div>
			<div class="arrow-container arrow-container-right jcarousel-next" id="arrow-white-r"></div>
			<div class="jcarousel jcarousel-setwidth-50">
				<ul class="jcarousel-list">
					<li class="jcarousel-item jcarousel-image active">
						<?php $index = 1; ?>
						<?php for ($i = $index; $i < $index+9; $i++): ?>
							<img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/>
						<?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+9; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+9; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+9; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+9; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+9; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+9; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+9; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+9; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+9; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+9; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+9; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
					<li class="jcarousel-item jcarousel-image">
						<?php for ($i = $index; $i < $index+6; $i++): ?><img class="gallery-item" src="./images/thumbs/th-<?=str_pad($i, 3, '0', STR_PAD_LEFT)?>-gta-tours-gallery.jpg"/><?php endfor; ?>
						<?php $index += 9; ?>
					</li>
				</ul>
			</div>
		</div>
		<div class="left-content-box right-content-box">
			<a href="#" class="gallery-big-link" data-featherlight="./images/pics/001-gta-tours-gallery.jpg">
				<img class="content-box-img gallery-big" src="./images/pics/001-gta-tours-gallery.jpg"/>
			</a>
		</div>
	</div>
	<div class="page-box page-box-h-2">
		<div class="vert-line vert-line-white vert-line-h-1"></div>
		<div class="down-arrow-grey down-arrow-white"></div>
	</div>
</div>
