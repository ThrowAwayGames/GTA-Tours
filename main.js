var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

/*Main Menu Btn functions*/
$(document).on("click", function(e){
	e.stopPropagation();
	$target=$(e.target);
	if(isMobile && $target.hasClass("menu-btn")===false && $target.parent().hasClass("nav-btn")===false){		
		$( ".nav" ).removeClass( "nav-open" );
	}
});
$( ".menu-btn" ).on( "click", function() {
	$( ".nav" ).toggleClass( "nav-open" );
});


$('.nav-btn a').click(function(e){
    e.preventDefault();
    var $href = $(this).attr('href');
    var $anchor = $($href).offset();
    window.scrollTo($anchor.left,$anchor.top);
    //return false;
});

// Gallery
$(".gallery-item").click(function (e) {
	var src = $(e.currentTarget).attr("src");
	src = "./images/pics/"+src.split("thumbs/th-")[1];
	$(".gallery-big").attr("src", src);
	$(".gallery-big-link").attr("data-featherlight", src);
});

// Packages More/Less
$(".package-row").on("click", ".btn-more", function (e) {
	var $btn = $(e.currentTarget);
	var $row = $(e.delegateTarget);
	var $text = $row.find(".pkg-box-text");
	var $icon = $row.find(".package-icon");

	if ($btn.hasClass("btn-less")) {
		//Hide full details
		$btn.removeClass("btn-less");
		$btn.text("MORE...");
		$text.css("height", "85px");
		$icon.css("height", "320px");
		if ($(window).width() > 1000) $row.css("height", "320px");
		else $row.css("height", "640px");
	} else {
		//Show full details
		$btn.addClass("btn-less");
		$btn.text("LESS...");
		$text.css("height", "auto");
		var h = $text.height();
		$icon.css("height", (h + 235) + "px");
		if ($(window).width() > 1000) $row.css("height", (h + 235) + "px");
		else $row.css("height", (h + 555) + "px");
	}
});

//Set up carousels
$(function() {
	//JCarousel inits
    $('.jcarousel').jcarousel({
		wrap: 'both',
		list: '.jcarousel-list',
		items: '.jcarousel-item'
    });

	$('.jauto').jcarouselAutoscroll({
        interval: 5000
	}).on('jcarousel:scroll', function(e, carousel, target, animate) {
		// "this" refers to the root element
		// "carousel" is the jCarousel instance
		// "target" is the target argument passed to the `scroll` method
		// "animate" is the animate argument passed to the `scroll` method
		//      indicating whether jCarousel was requested to do an animation

		var $wrapper = $("#testimonials");
		var index = $wrapper.find(".jcarousel-item.active").index();

		setActiveItem($wrapper, (index + 1) % $wrapper.find(".jcarousel-item").length, true);
	});

	//Per carousel events
	function setActiveItem ($wrapper, index, autoscroll) {
		//Set the active pagination item
		$wrapper.find(".jcarousel-pagination-item").removeClass("active");
		$($wrapper.find(".jcarousel-pagination-item").get(index)).addClass("active");

		//Set the active item
		$wrapper.find(".jcarousel-item").removeClass("active");
		$($wrapper.find(".jcarousel-item").get(index)).addClass("active");

		//Set jquery index
		if(!autoscroll)
			$wrapper.find(".jcarousel").jcarousel('scroll', index);
	}
	function carouselPrev (e) {
		var $wrapper = $(e.delegateTarget);
		var index = $wrapper.find(".jcarousel-item.active").index();
		var nextIndex = index - 1;
		if (nextIndex < 0) nextIndex = $wrapper.find(".jcarousel-item").length - nextIndex;

		setActiveItem($wrapper, (index - 1));
	}
	function carouselNext (e) {
		var $wrapper = $(e.delegateTarget);
		var index = $wrapper.find(".jcarousel-item.active").index();

		setActiveItem($wrapper, (index + 1) % $wrapper.find(".jcarousel-item").length);
	}
	$('.jcarousel-wrapper').on("click", ".jcarousel-prev", carouselPrev);
	$('.jcarousel-wrapper').on("click", ".jcarousel-next", carouselNext);
	if (isMobile){
		$('.jcarousel-wrapper').on("swipeleft", ".jcarousel", carouselNext);
		$('.jcarousel-wrapper').on("swiperight", ".jcarousel", carouselPrev);
	}
	$('.jcarousel-wrapper').on("click", ".jcarousel-pagination-item", function (e) {
		setActiveItem($(e.delegateTarget), $(e.currentTarget).index());
	});

	//Some items need manual width sizing
	function resizeItems () {
		var w = $(window).width()
		$(".jcarousel-setwidth .jcarousel-item").css("width", w + "px");
		if (w > 1000) $(".jcarousel-setwidth-50 .jcarousel-item").css("width", w/2 + "px");
		else $(".jcarousel-setwidth-50 .jcarousel-item").css("width", w + "px");
	}
	resizeItems();
	window.onresize = resizeItems;
});
