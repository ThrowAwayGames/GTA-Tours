<div class="container container-gray" id="contact">
	<div class="page-box page-box-h-2">
		<div class="vert-line vert-line-white vert-line-h-1"></div>
		<h1 class="page-title page-title-white">CONTACT</h1>
	</div>
	<div class="content-container-2">
		<div class="vert-line vert-line-white vert-line-h-1"></div>
		<h1 class="full-text-box">Peter Riedel, Photographer</h1>
		<h2 class="full-text-box">647-224-7177</h2>
		<h2 class="full-text-box text-lineheight-1">Head Office: Crown Hill Place, South Etobicoke, ON</h2>
		
		
		<div class="vert-line vert-line-white vert-line-h-2"></div>
		<h2 class="full-text-box">Cheryl, Assistant</h2>
		<h2 class="full-text-box text-lineheight-1">
			647-326-5690
		</h2>
		
		<div class="vert-line vert-line-white vert-line-h-1"></div>
		<div class="social-box">
			<a href="https://www.facebook.com/gtatoursca/" target="_blank" class="social-icon" id="social-icon-facebook"></a>
			<a href="https://twitter.com/prphoto1" target="_blank" class="social-icon" id="social-icon-twitter"></a>
			<a href="https://www.instagram.com/GTATOURS.CA/" target="_blank" class="social-icon" id="social-icon-google"></a>
			<a href="https://www.linkedin.com/in/peter-riedel-52429125" target="_blank" class="social-icon" id="social-icon-linkedin"></a>
			<a href="mailto:info@gtatours.ca" target="_blank" class="social-icon" id="social-icon-mail"></a>
		</div>
		
		<div class="vert-line vert-line-white vert-line-h-1"></div>
	</div>
	<div class="footer-box">
		<p class="footer-text footer-text-left">
			&copy; <?=date('Y')?> gtatours.ca
		</p>
		<p class="footer-text footer-text-right">
			<strong>Web Site by:</strong> <a href="http://throwawaygames.ca" class="footer-link">Throw Away Games Inc.</a>
		</p>
	</div>
</div>
