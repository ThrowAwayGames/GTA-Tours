<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=320, initial-scale=1">
<title>GTATours.ca</title>
<link href="jcarousel.css" rel="stylesheet" type="text/css">
<link href="featherlight.min.css" rel="stylesheet" type="text/css">
<link href="main.css" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Roboto:500,900,100,300,700,400' rel='stylesheet' type='text/css'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
<script src="jquery.jcarousel.min.js"></script>
<script src="jquery.jcarousel-autoscroll.min.js"></script>
<script src="featherlight.min.js"></script>
</head>
<body>
<?php require "menu.php";?>
<?php require "tour-block.php";?>
<?php require "services.php";?>
<?php require "gallery.php";?>
<?php require "packages.php";?>
<?php require "demo.php";?>
<?php require "testimonials.php";?>
<?php require "contact.php";?>
<script src="main.js"></script>
</body>
